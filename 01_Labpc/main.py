from http.server import HTTPServer, BaseHTTPRequestHandler
import json
from urllib.parse import urlparse, parse_qs
from io import BytesIO
import sys
from PowerControlTool import power_control

# create a instance power control device.
dev = power_control.powerDevice(format="json")


def DictValidate(dict_data, key, default=None):
    """Get value, if the key is contained in json else return default value.
    """
    if key in dict_data:
        return dict_data[key]
    else:
        return default


class LABPCHTTPHandler(BaseHTTPRequestHandler):
    """This class implementing a HTTP server on LAB PC.

    """

    def prepare_data(self, data):
        """Function support create a data from json data"""
        response = BytesIO()
        response.write(json.dumps(data).encode('utf-8'))
        return response.getvalue()

    def do_GET(self):
        """Handle a request by get method.           
        """
        self.send_response(200)
        self.send_header("Content-type", "application/json")
        self.end_headers()
        try:
            o = urlparse(self.path)
            if o.path == "/api/control/boards/":

                params = parse_qs(o.query)
                b = DictValidate(params, "board_pos")
                command = DictValidate(params, "control")
                b = int(b[0])
                command = command[0]
                if command.lower() not in ["on", "off", "boot", "unboot"]:
                    raise ValueError("Wrong input")
                if None in [b, command]:
                    raise ValueError("Wrong input")
                try:
                    ret = dev.boardControl(command, b)
                    data = {"ok": True, "message": "Look good"}
                except:
                    raise ValueError(
                        "get data from power control tool failed.")
                self.wfile.write(self.prepare_data(data))
            elif o.path == "/api/status/boards/":
                params = parse_qs(o.query)
                b = DictValidate(params, "board_pos")
                try:
                    ret = dev.boardControl("status", b)
                    data = {"ok": True, "data": ret}
                except Exception as e:
                    raise ValueError(
                        "get data from power control tool failed.")
                self.wfile.write(self.prepare_data(data))
            elif o.path == "/":
                data = {"ok": True, "data": "Server created successful!"}
                self.wfile.write(self.prepare_data(data))
            else:
                raise ValueError('Wrong url')
        except Exception as e:
            data = {"ok": False, "error": str(e)}
            self.wfile.write(self.prepare_data(data))


if __name__ == '__main__':
    host = 'localhost'
    port = 5000
    try:
        if len(sys.argv) == 2:
            x = str(sys.argv[1]).split(":")
            host = x[0]
            port = int(x[1])
        elif len(sys.argv) > 2:
            raise
    except:
        print("""
Wrong syntax, try: 
$python labserver.py [<host>:<port>]
default: host=localhost, port=5000
        """)
        exit(0)
    print("Server running on http://%s:%d/" % (host, port))

    httpd = HTTPServer((host, port), LABPCHTTPHandler)
    httpd.serve_forever()
