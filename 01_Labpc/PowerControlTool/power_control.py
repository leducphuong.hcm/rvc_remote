#!/usr/bin/env python3

import os
import json
import requests
import sys
from prettytable import PrettyTable
from enum import Enum


class Error(Enum):
    JSON_KEY = '1'
    BOARD_ID = '2'


def JsonValidate(key, json_data):
    '''Check if the key is contained in json
    '''
    if key in json_data:
        return json_data[key]
    else:
        return None


class GenTable():
    '''Generate the table format using for display purpose
    '''
    def __init__(self, title: list):
        self.table = PrettyTable(title)
        self.table.junction_char = "|"

    def add_row(self, data: list):
        row_data = []
        for arg in data:
            if arg is None:
                row_data.append("--")
            else:
                row_data.append(arg)
        self.table.add_row(row_data)

    def add_table(self):
        return self.table.get_string()


class smartDEN16R:
    """smartDEN IP-16R
    """

    ipAddr = None

    def __init__(self, ipAddr=None):
        """device initialise"""
        if ipAddr is not None:
            self.ipAddr = ipAddr

    def write(self, pin: int, value: int):
        """write state
        """
        url = "http://%s/current_state.json" % self.ipAddr
        power = "Relay%d" % pin
        params = {"pw": "admin", power: value}
        reply = requests.get(url=url, params=params)
        return reply

    def read(self, pin: int):
        """read state
        """
        url = "http://%s/current_state.json" % self.ipAddr
        params = {"pw": "admin"}
        reply = requests.get(url=url, params=params)
        relayInfo = json.loads(reply.text)
        relayStatus = relayInfo["CurrentState"]["Output"]

        return relayStatus[pin - 1]["Value"]


class powerDevice:
    """Power control device.

    Attr:
        classDir: path to this source file.
        configPath: path to config.json file.
        configData: data from config.json.

    """

    classDir = os.path.dirname(__file__)

    def __init__(self, path=None, format="table"):
        """Power device initialise.

        Arguments:
            path: path the config file (default: path to this source file).

        """

        if path is None:
            configPath = os.path.join(self.classDir, "config.json")
        else:
            configPath = os.path.join(path, "config.json")
        try:
            configFile = open(configPath, "r")
        except FileNotFoundError:
            print("Config file not found!")
            exit(1)
        self.configData = json.load(configFile)
        configFile.close()
        self.relayDevice = smartDEN16R()
        self.format = format

    def on(self, relayData, boardPos):
        """Power on.

        Arguments:
            relayData: Data of power control device.
            boardPos: position of board target in data of power control device.
        """
        self.unboot(relayData, boardPos)
        self.relayDevice.write(relayData["board_configuration"][boardPos]["power_relay_id"], 1)

    def off(self, relayData, boardPos):
        """Power off.

        Arguments:
            relayData: Data of power control device.
            boardPos: position of board target in data of power control device.
        """
        self.unboot(relayData, boardPos)
        self.relayDevice.write(relayData["board_configuration"][boardPos]["power_relay_id"], 0)

    def boot(self, relayData, boardPos):
        """boot.

        Arguments:
            relayData: Data of power control device.
            boardPos: position of board target in data of power control device.
        """
        self.relayDevice.write(relayData["board_configuration"][boardPos]["boot_relay_id"], 1)

    def unboot(self, relayData, boardPos):
        """unboot.

        Arguments:
            relayData: Data of power control device.
            boardPos: position of board target in data of power control device.
        """
        self.relayDevice.write(relayData["board_configuration"][boardPos]["boot_relay_id"], 0)

    def status(self, relayData, boardPos=-1):
        """status of a board or all boards.

        Arguments:
            relayData: Data of power control device.
            boardPos: position of board target in data of power control device.
        """

        table = GenTable(["ID", "Board Name", "Board Type", "Serial Number", "Power", "Boot"])
        database = []
        if type(relayData) is dict:
            relayData = [relayData]
        for relayIdx in range(len(relayData)):
            self.relayDevice.ipAddr = relayData[relayIdx]["ip_address"]
            boardConfig = JsonValidate("board_configuration", relayData[relayIdx])
            if boardPos != -1:
                boardConfig = [boardConfig[boardPos]]
            for boardIdx in range(len(boardConfig)):
                boardName = JsonValidate("board_name", boardConfig[boardIdx])
                boardID = JsonValidate("board_id", boardConfig[boardIdx])
                boardType = JsonValidate("board_type", boardConfig[boardIdx])
                serialNumber = JsonValidate("serial_number", boardConfig[boardIdx])
                powerID = JsonValidate("power_relay_id", boardConfig[boardIdx])
                bootID = JsonValidate("boot_relay_id", boardConfig[boardIdx])
                if self.relayDevice.read(powerID) == "0":
                    powerStatus = "off"
                else:
                    powerStatus = "on"
                if self.relayDevice.read(bootID) == "0":
                    bootStatus = "unboot"
                else:
                    bootStatus = "boot"
                data = [boardID, boardName, boardType, serialNumber, powerStatus, bootStatus]
                table.add_row(data)
                tmp = dict()
                title = ["board_pos", "name", "type", "SerialNumber", "power", "boot"]
                for idx in range(len(title)):
                    tmp[title[idx]] = data[idx]
                database.append(tmp)

        if self.format == "table":
            print(table.add_table())
            return 0
        elif self.format == "json":
            return database

    def list(self):
        """Execute command to list all board in the labPC
        """
        board_table = GenTable(["ID", "Board Name", "Board Type", "Serial Number", "PCD IP Adress"])
        json_key = "power_device"
        power_device_list = JsonValidate(json_key, self.configData)
        if power_device_list is None:
            return Error.JSON_KEY.value
        for power_device in power_device_list:
            # pcd_name = JsonValidate("name", power_device)
            pcd_ip = JsonValidate("ip_address", power_device)
            if pcd_ip is None:
                return Error.JSON_KEY.value
            board_info_list = JsonValidate("board_configuration", power_device)
            if board_info_list is None:
                return Error.JSON_KEY.value
            for board_info in board_info_list:
                board_name = JsonValidate("board_name", board_info)
                board_id = JsonValidate("board_id", board_info)
                board_type = JsonValidate("board_type", board_info)
                serial_number = JsonValidate("serial_number", board_info)
                board_table.add_row([board_id, board_name, board_type, serial_number, pcd_ip])
        print(board_table.add_table())
        return 0

    def boardControl(self, command, boardID):
        """Control a specific board by board ID.

        Arguments:
            command: The command to used to control the board target.
            boardID: ID of board target.
        """

        relayData = self.configData["power_device"]
        if command == "list":
            ret = self.list()
            return ret
        elif boardID is not None:
            relayPos = -1
            boardPos = -1
            for relayIdx in range(len(relayData)):
                for boardIdx in range(len(relayData[relayIdx]["board_configuration"])):
                    if relayData[relayIdx]["board_configuration"][boardIdx]["board_id"] == int(boardID):
                        relayPos = relayIdx
                        boardPos = boardIdx

            if relayPos == -1 or boardPos == -1:
                print("Cannot found board ID!")
                return Error.BOARD_ID.value

            self.relayDevice.ipAddr = relayData[relayIdx]["ip_address"]
            control = getattr(self, command)
            ret = control(relayData[relayPos], boardPos)
            return ret
        else:
            control = getattr(self, command)
            ret = control(relayData)
            return ret


class CommandHelper():
    def Help(self):
        print("Lab PC standalone tool")
        print("1. Help command:")
        print("    python power-control.py [-h/--help]")
        print("2. List command:")
        print("    python power-control.py [-l/--list]")
        print("3. Status command:")
        print("    python power-control.py [-s/--status]")
        print("4. Control command:")
        print("    python power-control.py [-b/--board] <board_id> <command>")

    def Control(self):
        print("Control board command")
        print("    python power-control.py [-b/--board] <board_id> <command>")
        print("        board_id: Use list command to get")
        print("        command: [on/off/boot/unboot/status]")


if __name__ == "__main__":
    printer = CommandHelper()
    command = None
    board_id = None
    if len(sys.argv) == 1:
        printer.Help()
        exit(0)
    elif len(sys.argv) == 2:
        if sys.argv[1] in ["-h", "--help"]:
            printer.Help()
            exit(0)
        elif sys.argv[1] in ["-l", "--list"]:
            command = "list"
        elif sys.argv[1] in ["-s", "--status"]:
            command = "status"
        else:
            printer.Help()
            exit(0)
    elif len(sys.argv) == 3:
        printer.Help()
    elif len(sys.argv) == 4:
        if sys.argv[1] not in ["-b", "--board"]:
            printer.Control()
        if sys.argv[3] not in ["on", "off", "boot", "unboot", "status"]:
            printer.Control()
        else:
            command = sys.argv[3]
            board_id = sys.argv[2]

    dev = powerDevice()
    ret = dev.boardControl(command, board_id)
    exit(ret)
