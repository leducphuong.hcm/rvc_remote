
from flask import Flask, request
from PowerControlTool import power_control
import sys


app = Flask(__name__)
# create a instance power control device.
dev = power_control.powerDevice(format="json")


@app.route("/api/control/boards/", methods=['GET'])
def control():
    """Handle a request to control board. 

    Method: GET

    Arguments   
    board_pos    (int)   required
    command      (str)   required

    Returns:
        json data.

    """
    try:
        b = request.args.get("board_pos", None)
        command = request.args.get("control", None)
        if None in [b, command]:
            raise ValueError("Wrong input")
        if command.lower() not in ["on", "off", "boot", "unboot"]:
            raise ValueError("Wrong command input")
        try:
            b = int(b)
        except:
            raise ValueError("Wrong board_pos input")
        try:
            dev.boardControl(command, b)
            data = {"ok": True, "message": "Look good"}
        except:
            raise ValueError("Power control failed.")
        return data
    except Exception as e:
        return {"ok": False, "error": str(e)}


@app.route("/api/status/boards/", methods=['GET'])
def status():
    """Handle a request to get status of boards. 

    Method: GET

    Arguments   
    board_pos    (int)   optional

    Returns:
        json data.

    """
    try:
        b = request.args.get("board_pos", None)
        if b != None:
            try:
                b = int(b)
            except:
                raise ValueError("Wrong board_pos input")
        try:
            ret = dev.boardControl("status", b)
            data = {"ok": True, "data": ret}
        except:
            raise ValueError("Power control failed.")
        return data
    except Exception as e:
        return {"ok": False, "error": str(e)}


@app.route("/")
def home():
    return "LABPC Server created successful!\n"


if __name__ == '__main__':
    HOST = 'localhost'
    PORT = 5000
    try:
        if len(sys.argv) == 2:
            x = str(sys.argv[1]).split(":")
            HOST = x[0]
            PORT = int(x[1])
        elif len(sys.argv) > 2:
            raise
    except:
        print("""
Wrong syntax, try: 
$python labmain.py [<host>:<port>]
default: host=localhost, port=5000
        """)
        exit(0)
    app.run(debug=True, host=HOST, port=PORT)
