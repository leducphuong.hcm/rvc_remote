# mysite/urls.py
from django.conf.urls import include
from django.urls import path
from django.contrib import admin
from django.views.generic.base import RedirectView
from django.urls import reverse_lazy

urlpatterns = [
    path('myboard/', include('myboard.urls')),
    path('admin/', admin.site.urls),
    path('',RedirectView.as_view(url=reverse_lazy('admin:index'))),
]
