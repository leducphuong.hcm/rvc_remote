from django.db import models
from django.contrib.auth.models import BaseUserManager
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.utils.translation import gettext_lazy as _
from django.utils import timezone
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.apps import apps
from django.contrib.auth.hashers import make_password


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


class MyUserManager(BaseUserManager):

    def _create_user(self, username, email, password, **extra_fields):
        """
        Create and save a user with the given username, email, and password.
        """
        if not username:
            raise ValueError('The given username must be set')
        email = self.normalize_email(email)
        # Lookup the real model class from the global app registry so this
        # manager method can be used in migrations. This is fine because
        # managers are by definition working on the real model.
        GlobalUserModel = apps.get_model(
            self.model._meta.app_label, self.model._meta.object_name)
        username = GlobalUserModel.normalize_username(username)
        user = self.model(username=username, email=email, **extra_fields)
        user.password = make_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, username, email=None, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        extra_fields.setdefault('is_labpc', False)
        return self._create_user(username, email, password, **extra_fields)

    def create_superuser(self, username, email=None, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_labpc', False)
        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')
        return self._create_user(username, email, password, **extra_fields)


class CustomUser(AbstractBaseUser, PermissionsMixin):
    """
    An abstract base class implementing a fully featured User model with
    admin-compliant permissions.

    Username and password are required. Other fields are optional.
    """
    class Meta:
        verbose_name_plural = "Accounts"
    username_validator = UnicodeUsernameValidator()

    username = models.CharField(
        _('username'),
        max_length=150,
        unique=True,
        help_text=_(
            'Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
        validators=[username_validator],
        error_messages={
            'unique': _("A user with that username already exists."),
        },
    )
    first_name = models.CharField(_('first name'), max_length=150, blank=True)
    last_name = models.CharField(_('last name'), max_length=150, blank=True)
    email = models.EmailField(_('email address'), blank=True)
    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_(
            'Designates whether the user can log into this admin site.'),
    )
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )
    is_labpc = models.BooleanField(
        default=False,
        help_text=_(
            'User for LabPC'
        ),
    )
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

    objects = MyUserManager()

    EMAIL_FIELD = 'email'
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    def clean(self):
        super().clean()
        self.email = self.__class__.objects.normalize_email(self.email)

    def get_full_name(self):
        """
        Return the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        """Return the short name for the user."""
        return self.first_name


class LABPC(models.Model):
    """Class description LABPC properties.

    Attributes:
        user : LABPC assigned to a account.
        username: username of LABPC
        host: IP address of device.
        port: port number.
    """
    username_validator = UnicodeUsernameValidator()
    user = models.OneToOneField(
        CustomUser, on_delete=models.CASCADE, editable=False, null=True, blank=True)
    username = models.CharField(
        null=True,
        max_length=150,
        unique=True,
        help_text=_('''Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only. <br> 
        An account with username same usermane LABPPC will created, that help LABPC have access to the server when needed.<br>
        Example: lab_automation
'''),
        validators=[username_validator],
        error_messages={
            'unique': _("A user with that username already exists."),
        },)
    host = models.CharField(max_length=255, null=True,
                            help_text=_('Example: 192.168.1.10'))
    port = models.IntegerField(default=5000, null=True)

    def __str__(self):
        return "(%s) labpc_%s" % (str(self.id), self.username)


@receiver(post_save, sender=LABPC)
def save_user_name(sender, instance=None, created=False, **kwargs):
    if created:
        try:
            c = CustomUser.objects.get(username=instance.username)
        except CustomUser.DoesNotExist:
            c = CustomUser.objects.create(
                username=instance.username, is_labpc=True)
        instance.user = c
        instance.save()


class PCD(models.Model):
    """Class description power control device properties.

    Attributes:
        type: type of device.
        host: IP address of device.
        port: port number.
        LABPCOwner: owner of Lab PC.

    """

    name = models.CharField(max_length=255)

    class Type(models.IntegerChoices):
        """type of power control device.
        """
        Unknown = 0
        SmartDEN_IP_16R = 1

    type = models.IntegerField(
        default=Type.SmartDEN_IP_16R, choices=Type.choices)
    host = models.CharField(max_length=255, null=True, blank=True, help_text=_(
        'IP address of relay device.'))
    port = models.IntegerField(
        default=0, blank=True, help_text=_('Port number of relay device.'))
    LABPCOwner = models.ForeignKey(LABPC, on_delete=models.CASCADE, null=True, editable=False,help_text=_(
        'The LABPC this PCD belongs to.'))

    def __str__(self):
        return "(%s) pcd_%s_%s" % (str(self.id), self.name, self.LABPCOwner.username)


class Board(models.Model):
    """Class description Board properties.

    Attributes:
        displayName: board name.
        boardPos: Position/ID of board in labPC.
        boardType: Type of board
        usbSerialNumber : usb serial number of board
        PCDOwner: owner of power control device.
        LABPCOwner: owner of lab PC.
        powerSwitchRelayNumber: number of relay for power switch.
        accSwitchRelayNumber: number of relay for acc switch.
        isActivate:  Property indicates board already to use.
        isShareControl: Property indicates board is sharing control.
        occupy: Property indicates whether the board is in use.
        usingBy: Property indicates who is using the board.

    """

    UNK = 'Unknown'
    S4 = 'S4'
    V3H1 = 'V3H1'
    V3H2 = 'V3H2'
    V3U = 'V3U'
    BOARD_TYPE_CHOICES = [
        (UNK, 'Unknown'),
        (S4, 'Spider'),
        (V3H1, 'Condor'),
        (V3H2, 'Condor-I'),
        (V3U, 'Falcon')
    ]

    def __str__(self):
        return "(%s) %s" % (str(self.id), self.displayName)

    class StatusPower(models.IntegerChoices):
        """type of power status.
        """

        POWER_OFF = 0
        POWER_ON = 1

    class StatusBoot(models.IntegerChoices):
        """type of power status.
        """

        UNBOOT = 0
        BOOT = 1

    displayName = models.CharField(
        max_length=255, help_text=_('Example: my_spider_board'))
    boardPos = models.IntegerField(
        default=-1, help_text=_('Board position is unique in a LABPC.'))
    boardType = models.CharField(
        max_length=255, choices=BOARD_TYPE_CHOICES, default=UNK)
    usbSerialNumber = models.CharField(
        max_length=255, default=None, null=True, blank=True, help_text=_('USB serial number.'))
    boardUniqueSerialNumber = models.CharField(
        max_length=255, default=None, unique=True, null=True, blank=True, help_text=_('Unique serial number of board.'))
    PCDOwner = models.ForeignKey(PCD, on_delete=models.CASCADE,editable=False, null=True, blank=True)
    LABPCOwner = models.ForeignKey(LABPC, on_delete=models.CASCADE)
    holders =  models.ManyToManyField(CustomUser,limit_choices_to={'is_labpc': False},related_name='boards', null=True, blank=True)
    powerSwitchRelayNumber = models.IntegerField(default=-1)
    accSwitchRelayNumber = models.IntegerField(default=-1)
    isActivate = models.BooleanField(
        default=True, help_text=_('Indicates board already to use.'))
    occupy = models.BooleanField(default=False, editable=False)
    isShareControl = models.BooleanField(
        default=True, help_text=_('Indicates board is sharing control.'))
    usingBy = models.CharField(
        default=None, max_length=255, editable=False, null=True, blank=True)
    statusPower = models.IntegerField(
        default=StatusPower.POWER_OFF, choices=StatusPower.choices)
    statusBoot = models.IntegerField(
        default=StatusBoot.UNBOOT, choices=StatusBoot.choices)


@receiver(post_save, sender=Board)
def board_save_signal(sender, instance=None, created=False, **kwargs):
    if created:
        instance.save()
