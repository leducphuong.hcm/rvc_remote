from pyexpat import model
from rest_framework import serializers
from .models import Board, CustomUser, LABPC


class HoldersSerializer(serializers.ModelSerializer):
    """Generate json format for Holders"""

    class Meta:
        model = CustomUser
        fields = ['username']


class LabPCsSerializer(serializers.ModelSerializer):
    """Generate json format for LabPCs"""

    class Meta:
        model = LABPC
        fields = ['id','host','port','username']


class GetBoardsSerializer(serializers.ModelSerializer):
    """Generate json format for Boards"""

    def to_representation(self, board):
        print("Serializing board:", end="")
        print(board)
        holders = HoldersSerializer(board.holders.all(),read_only=True, many=True).data
        labpcs = LabPCsSerializer(board.LABPCOwner,read_only=True).data
        return {
            'board_id': board.id,
            'board_pos': board.boardPos,
            'name': board.displayName,
            'type': board.boardType,
            'status_power': "ON" if board.statusPower == Board.StatusPower.POWER_ON else "OFF",
            'status_boot': "BOOT" if board.statusBoot == Board.StatusBoot.BOOT else "UNBOOT",
            'LABPC': labpcs,
            'power_relay_number': board.powerSwitchRelayNumber,
            'acc_relay_number': board.accSwitchRelayNumber,
            'using_by': board.usingBy if board.usingBy else "Unknown",
            'occupy': board.occupy,
            'sharing': board.isShareControl,
            'holders': holders,
        }
