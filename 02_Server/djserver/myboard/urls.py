from django.urls import path
from . import views

urlpatterns = [
    path('api/v1/status/boards/share/', views.GetShareBoardsAPI.as_view(),name="board_status"),  # status shared board API
    path('api/v1/control/boards/share/', views.SetControlBoardAPI.as_view()),  # control board API
]
