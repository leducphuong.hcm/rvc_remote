from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .serializers import GetBoardsSerializer
from .models import Board, LABPC
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import TokenAuthentication
from django.core.exceptions import ObjectDoesNotExist
import requests


def set_if_not_none(mapping, key, value):
    """Set value for dict mapping if value not None.
    """
    if value is not None:
        mapping[key] = value


def get_status_control(control: str):
    """Return board status.
    """
    control = control.lower()
    if control in ['on']:
        return Board.StatusPower.POWER_ON
    elif control in ['off']:
        return Board.StatusPower.POWER_OFF
    elif control in ["boot"]:
        return Board.StatusBoot.BOOT
    elif control in ["unboot"]:
        return Board.StatusBoot.UNBOOT


class GetShareBoardsAPI(APIView):
    """Get list of shared boards."""

    authentication_classes = [SessionAuthentication,
                              BasicAuthentication, TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request):
        print("Get list share board by ", end="")
        print(request.user)
        try:
            sort_board_id = request.GET.get('board_id', None)
            sort_using_by = request.GET.get('using_by', None)
            sort_labpc_id = request.GET.get('labpc_id', None)
            sort_holder_username = request.GET.get('holder_username', None)
            sort_board_type = request.GET.get('board_type', None)
            sort_board_pos = request.GET.get('board_pos', None)
            need_update_value = request.GET.get('update_value', True)
            if (
                hasattr(need_update_value, 'lower')
                and need_update_value.lower() in ['false']
            ):
                need_update_value = False
            # The params sort by
            sort_params = {}
            set_if_not_none(sort_params, 'id', sort_board_id)
            set_if_not_none(sort_params, 'usingBy', sort_using_by)
            set_if_not_none(sort_params, 'LABPCOwner__id', sort_labpc_id)
            set_if_not_none(sort_params, 'holders__username', sort_holder_username)
            set_if_not_none(sort_params, 'boardType', sort_board_type)
            set_if_not_none(sort_params, 'boardPos', sort_board_pos)

            holder=request.user
            bs = Board.objects.filter(
                sort_params,  isShareControl=True, isActivate=True)
            if holder != None:
                bhs = Board.objects.filter(
                    sort_params,  isShareControl=False, isActivate=True, holders=holder)
                if bhs.exists():
                    bs = bs | bhs
            data = {}
            data["error"] = []
            if need_update_value == True:
                # to avoid having to repeat the LAPBC requests many times.
                labPCRequested = []
                for b in bs:
                    if b.LABPCOwner.id in labPCRequested:
                        continue
                    url = "http://%s:%d/api/status/boards/?" % (
                        str(b.LABPCOwner.host), int(b.LABPCOwner.port))
                    try:
                        r = requests.get(url, timeout=5).json()
                    except:
                        r = {"ok": False, "error": "Board [%d:%s] in LAPBC[%s] not respond" % (
                            b.id, b.displayName, str(b.LABPCOwner.username))}
                    if r.get("ok"):
                        labPCRequested.append(b.LABPCOwner.id)
                        boardsData = r.get("data")
                        for board in boardsData:
                            try:
                                _b = Board.objects.get(
                                    LABPCOwner__id=b.LABPCOwner.id, boardPos=board["board_pos"])
                                if board["power"] is not None:
                                    _b.statusPower = get_status_control(
                                        board["power"])
                                if board["boot"] is not None:
                                    _b.statusBoot = get_status_control(
                                        board["boot"])
                                _b.save()
                            except Board.DoesNotExist:
                                print("LAPBC[{0} ip={2}] not exist boardPos[{1}]".format(
                                    b.LABPCOwner.id, board["board_pos"], b.LABPCOwner.host))
                            except:
                                print("LAPBC Respose wrong format")
                    else:
                        data["error"].append(
                            {"board_id": b.id, "error": r.get("error")})
                bs = Board.objects.filter(
                    sort_params,  isShareControl=True, isActivate=True).order_by('-id')
                if holder != None:
                    bhs = Board.objects.filter(
                        sort_params,  isShareControl=False, isActivate=True, holders=holder).order_by('-id')
                    if bhs.exists():
                        bs = bs | bhs

            if len(data["error"]) > 0:
                data["ok"] = False
            else:
                data["ok"] = True
            mydata = GetBoardsSerializer(bs, many=True)
            data["boards"] = mydata.data
            return Response(data, status=status.HTTP_200_OK)
        except Exception as e:
            return Response({"ok": False, "error": str(e)}, status=status.HTTP_408_REQUEST_TIMEOUT)


class SetControlBoardAPI(APIView):
    """Class receives requests from client and executes control boards."""

    authentication_classes = [SessionAuthentication,
                              BasicAuthentication, TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request):
        print("request from " + str(request.user.username)
              if request.user.id else "request from anonymous")
        try:
            resp = {"ok": True, "message": "Look good"}
            sort_board_id = request.GET.get('board_id', None)
            sort_labpc_id = request.GET.get('labpc_id', None)
            sort_board_pos = request.GET.get('board_pos', None)
            sort_relay_id = request.GET.get('relay_id', None)
            using_by = request.GET.get('using_by', None)
            occupy = request.GET.get('occupy', None)
            control = request.GET.get("control", None)  # on or off

            if control is None or control.lower() not in ['off', 'on', 'boot', 'unboot']:
                raise ValueError("Need input control=on/off/boot/unboot")

            if None not in [sort_board_pos, sort_labpc_id] and all(ele == None for ele in [sort_relay_id, sort_board_id]):
                # lab id and board pos of lab
                # authentication user has permition pos -> labpc
                try:
                    l = LABPC.objects.get(id=int(sort_labpc_id))
                    b = Board.objects.filter(
                        LABPCOwner=l, boardPos=sort_board_pos).order_by('-id')
                except ObjectDoesNotExist:
                    raise ValueError("Labpc {0} is not exist or Board ID {1} in LABPC[{0}] not exist.".format(
                        sort_labpc_id, sort_board_pos))
                url = "http://%s:%d/api/control/boards/" % (
                    str(l.host), int(l.port))
                params = {}
                set_if_not_none(params, 'cmd', 'control')
                set_if_not_none(params, 'control', control)
                set_if_not_none(params, 'board_pos', sort_board_pos)
                set_if_not_none(params, 'labpc_id', sort_labpc_id)
                try:
                    r = requests.get(url, params=params, timeout=5).json()
                    resp["data"] = r
                except:
                    raise ValueError("Failed request to %s" % url)
                return Response(resp, status=status.HTTP_200_OK)            

            if sort_board_id is None:
                raise ValueError("Need more input: board_id")
            try:
                b = Board.objects.get(id=int(sort_board_id))
            except Board.DoesNotExist:
                raise ValueError("Board is not exist")
            except:
                raise ValueError("Failed get board")

            # validate board
            if not b.isActivate:
                raise ValueError("Board is not activate")
            if not b.isShareControl:
                if request.user not in b.holders.all():
                    raise ValueError("Board is not sharing")

            # lock board implement here
            if b.occupy:
                if b.usingBy == using_by:
                    if occupy is not None and occupy.lower() in ["give"] and control in ["off"]:
                        b.occupy = False
                        b.usingBy = None
                        b.save()
                    elif occupy is not None and occupy.lower() in ["give"]:
                        raise ValueError(
                            "Board is using by %s. Just 'give' when off board" % (b.usingBy))
                else:
                    raise ValueError(
                        "Board is using by %s. Unlock board first." % (b.usingBy))
            else:  # take when power on, give when power off.
                if occupy is not None and occupy.lower() in ["take"]:
                    if using_by is None:
                        raise ValueError(
                            "Board is using by %s.  Need input using_by." % (b.usingBy))
                    elif control not in ["on"]:
                        raise ValueError(
                            "Board is using by %s.  Just 'take' when on board." % (b.usingBy))
                    elif using_by is not None and control in ["on"]:
                        b.occupy = True
                        b.usingBy = using_by
                        b.save()
                    else:
                        raise ValueError(
                            "Board is using by %s. Need more input." % (b.usingBy))

            url = "http://%s:%d/api/control/boards/?" % (
                str(b.LABPCOwner.host), int(b.LABPCOwner.port))
            params = {}
            set_if_not_none(params, 'cmd', 'control')
            set_if_not_none(params, 'control', control)
            set_if_not_none(params, 'board_pos', b.boardPos)
            set_if_not_none(params, 'labpc_host', b.LABPCOwner.host)
            set_if_not_none(params, 'labpc_port', b.LABPCOwner.port)
            set_if_not_none(params, 'power_relay_id', b.powerSwitchRelayNumber)
            set_if_not_none(params, 'boot_relay_id', b.accSwitchRelayNumber)
            try:
                # request to LABPC
                r = requests.get(url, params=params, timeout=5).json()
            except:
                raise ValueError("Cannot connect to LABPC")
            return Response(r, status=status.HTTP_200_OK)
        except Exception as e:
            return Response({"ok": False, "error": str(e)}, status=status.HTTP_400_BAD_REQUEST)
