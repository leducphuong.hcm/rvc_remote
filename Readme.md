# Remote Cloud System

## Setup

The first thing to do is to clone the repository.

### Server

Create a virtual environment to install dependencies in and activate it:

```sh
$python3 -m venv .env
$source remote\bin\activate
```

Then install the dependencies:

```sh
(.env)$pip install -r requirements.txt
```

Note the `(.env)` in front of the prompt. This indicates that this terminal
session operates in a virtual environment set up by `venv`.

Once `pip` has finished downloading the dependencies:

```sh
(.env)$cd djserver
(.env)$python manage.py migrate
(.env)$python manage.py runserver
```

And navigate to http://127.0.0.1:8000/myboard/.

### LabPC host

Create a virtual environment to install dependencies in and activate it:

```sh
$python3 -m venv .env
$source remote\bin\activate
```

Then install the dependencies:

```sh
(.env)$pip install -r requirements.txt
```

Once `pip` has finished downloading the dependencies:

```sh
(.env)$python labmain.py
```

Default LabcPC run on 127.0.0.1:5000.
If you wand run on specify host:

```sh
(.env)$python labmain.py <host>:<port>
```

### Powe Control Tool

Connect all the target boards to relay modules on a LabPC, then users need to create a JSON file that describes this information, for example:

```

{
    "labpc_ip": "192.168.5.192",
    "power_device": [
        {
            "name": "smartDEN-IP-16R-MT",
            "ip_address": "192.168.5.250",
            "board_configuration": [
                {
                    "board_id": 1,
                    "board_name": "falcon",
                    "board_type": "V3U",
                    "serial_number": "000001",
                    "power_relay_id": 4,
                    "boot_relay_id": 3
                },
                {
                    "board_id": 2,
                    "board_name": "spider",
                    "board_type": "S4",
                    "serial_number": "000002",
                    "power_relay_id": 11,
                    "boot_relay_id": 12
                }
        }
}
```

Test power control tool:

```sh
$python power_control.py --help
$python power_control.py -l
$python power_control.py -s
$python power_control.py -b 1 on
```

## User PC

Change config.json

```
{
    "server":
    {
        "port":"8000",
        "url":"http://127.0.0.1"
    },
    "authentication":
    {
        "token":"<token of user to access server>"
    }
}
```

Run main file to show detail help.

```text
python main.py -h
```

___

## Usage

### At Server:

* Create Administrator

```sh
$ python manage.py createsuperuser
Username: admin
Email address:         
Password: 
Password (again):
Superuser created successfully.
```

* Login Admin page with username and password created:
http://127.0.0.1:8000/admin/

* Create user:
http://127.0.0.1:8000/admin/myboard/customuser/add

* Get token : https://127.0.0.1:8000/admin/authtoken/tokenproxy/

* Create labpc, and board: https://127.0.0.1:8080/admin.
  
### At User PC:

* Control:

    The main structure of the command:

    ```sh
    $python main.py <command> [<args…>] [<options>]
    ```

    Type 'main.py \<command> --help' for help on a specific command.
    Available \<command>

        list        List boards in system.
        status      Get status available boards.
        on          Power on board
        off         Power off board.
        boot        Into boot board.
        unboot      Into unboot board.


    List all board:

    ```sh
    $ python main.py list
    +-----------+--------+------+---------------+
    | Board UID |  Name  | Type |   LABPC[ID]   |
    +-----------+--------+------+---------------+
    |     1     | spider |  S4  | automation[1] |
    |     2     | falcon | V3U  | automation[1] |
    +-----------+--------+------+---------------+
    ```

    Turn on board with board unique id (board_id=1)

    ```sh
    $ python main.py on 1
    {'ok': True, 'message': 'Look good'}
    ```

    Turn off board with board unique id (board_id=1):

    ```sh
    $ python main.py off 1
    {'ok': True, 'message': 'Look good'}
    ```

    To display status boards:

    ```sh
    $ python main.py status

    +-----------+--------+------+---------------+-------+--------+
    | Board UID |  Name  | Type |   LABPC[ID]   | Power |  Boot  |
    +-----------+--------+------+---------------+-------+--------+
    |     2     | falcon | V3U  | automation[1] |   ON  |  BOOT  |
    |     1     | spider |  S4  | automation[1] |   OFF | UNBOOT |
    +-----------+--------+------+---------------+-------+--------+
    ```

## Ref
___

* https://docs.djangoproject.com/en/3.2/

___
Copyright (C) 2021 Renesas Electronics Corporation. All rights reserved.
