#!../remote/bin/python
"""
***********************************************************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
* other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
* applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
* EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
* SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS
* SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
* this software. By using this software, you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2021 Renesas Electronics Corporation. All rights reserved.
***********************************************************************************************************************
***********************************************************************************************************************
* File Name    : main.py
* Version      : 0.1.0
* Product Name : Remote Cloud
* Device(s)    : N/A
* Description  : Main script for User CLI using.
***********************************************************************************************************************
***********************************************************************************************************************
* History   Version   DD.MM.YYYY   Description
*           0.1.0     13.12.2021   Initial revision.
***********************************************************************************************************************
"""
# import from standard libraries
import argparse
import requests
import sys
import json
from prettytable import PrettyTable
import os

JUNCTION_CHAR = '+'  # for PrettyTable
# URL API Server
URL_STATUS_BOARD_SHARE = 'myboard/api/v1/status/boards/share/?'
URL_CONTROL_BOARD_SHARE = 'myboard/api/v1/control/boards/share/?'


def set_if_not_none(mapping, key, value):
    """Add key value if value not none.

    """
    if value is not None:
        mapping[key] = value


def get_value_validate(json_data, key, default=None):
    """Get value if the key is contained in json data, else return default value.
    """
    return json_data.get(key, default)


class Config():
    """Load config of user from json file. Build URLs for connection.
    """

    def __init__(self, path=None):
        self.classDir = os.path.dirname(__file__)
        if path is None:
            configPath = os.path.join(self.classDir, "config.json")
        else:
            configPath = os.path.join(path, "config.json")
        try:
            self.configFile = open(configPath, "r")
        except FileNotFoundError:
            print("Config file not found!")
            exit(1)
        self.configData = json.load(self.configFile)
        self.configFile.close()
        try:
            self.port = self.configData["server"]["port"]
            self.url = self.configData["server"]["url"]
            self.host = self.url + ":" + self.port + "/"
            self.url_status_board_share = self.host + URL_STATUS_BOARD_SHARE
            self.url_control_board_share = self.host + URL_CONTROL_BOARD_SHARE
            self.headers = {'Authorization': "Token %s" % (
                self.configData["authentication"]["token"])}
        except:
            print("Can't read config.json file")
            exit(1)


class RemoteCLIArgumentParser(object):
    """Parsing argument and execute the command

    Method(s):
        list: Execute command to find the expected board list by using filter
        status: Execute command to find the expected board status by using filter
        control : Execute command to control the board's power/boot state
    """

    def __init__(self):
        """InitInitial parameterized constructor
        """
        parser = argparse.ArgumentParser(
            usage='''<command> [<args>] ...

Type 'main.py <command> --help' for help on a specific command.

Available <command>

    list        List boards in system.
    status      Get status available boards.
    on          Power on board
    off         Power off board. 
    boot        Into boot board.
    unboot      Into unboot board.


''')
        self.cfg = Config()
        parser.add_argument('command', help='command to run')
        args = parser.parse_args(sys.argv[1:2])
        if args.command in ['on', 'off', 'boot', 'unboot']:
            getattr(self, 'control')()
        elif not hasattr(self, args.command):
            print('Unrecognized command')
            parser.print_help()
            exit(1)
        else:
            getattr(self, args.command)()

    def list(self):
        """Execute command to find the expected board list by using filter

        """
        parser = argparse.ArgumentParser(
            usage='''list [<filter>: [-t/--board-type] [-l/--labpc-id] [-h]]
                          [-h/--help] [-f/--format] [-e]

Display list boards is sharing on board farm.

Example: 
list                List all board is sharing on board farm.

with some <filter>:
list -l 2           List all board on specific labpc(labpc_id=2).
list -t S4          List all board with specific type board(board_type=S4).
list -l 2 -t V3H     List all board on specific labpc and type board.

''')
        parser.add_argument('-l', '--labpc-id', help='Add LabPC ID into search parameters')
        parser.add_argument('-t', '--board-type', help='Add board type ijto search parameters')
        parser.add_argument('-u', '--using-by', nargs='?',
                            const=os.getlogin(), help='Indicates who the device is used by.')
        parser.add_argument('-n', dest = 'holder_username', help='Add Holder username into search parameters.')
        parser.add_argument('-f', '--format', default='table',
                            help='format type: json or table, default is table')
        parser.add_argument('-e', dest = "exp", help='Display expand data',
                            nargs='?', const=True, default=False)

        args = parser.parse_args(sys.argv[2:])

        params = {}
        set_if_not_none(params, "labpc_id", args.labpc_id)
        set_if_not_none(params, "board_type", args.board_type)
        set_if_not_none(params, "using_by", args.using_by)
        set_if_not_none(params, "holder_username", args.holder_username)
        set_if_not_none(params, "update_value", False)
        try:
            try:
                resp = requests.get(self.cfg.url_status_board_share,
                                    params=params, headers=self.cfg.headers).json()
            except Exception as e:
                raise Exception("Cannot connect to server. Change settings in config.json and try again.")
            ok = resp.get("ok", None)
            if ok is None:
                raise Exception(str(resp))
            if ok:
                bs = resp.get("boards")
                if args.format == 'json':
                    print(json.dumps(bs, indent=4, sort_keys=True))
                else:
                    x = PrettyTable()
                    x.junction_char = JUNCTION_CHAR
                    if args.exp:
                        x.field_names = ["Board UID", "Name", "Type",
                                         "LABPC[ID]", "LABPC Address", "Users", "Board Pos"]
                        for b in bs:
                            LABPC = get_value_validate(b, "LABPC")
                            holders = get_value_validate(b, "holders")
                            holders = [h['username'] for h in holders]
                            # print(type(holders))
                            # print(holders)
                            x.add_row([get_value_validate(b, "board_id"), get_value_validate(b, "name"), get_value_validate(b, "type"),
                                       "%s[%s]" % (get_value_validate(
                                           LABPC, "username"), get_value_validate(LABPC, "id")),
                                       "%s:%s" % (get_value_validate(
                                           LABPC, "host"), get_value_validate(LABPC, "port")),
                                        holders,
                                       get_value_validate(b, "board_pos")])
                    else:
                        x.field_names = ["Board UID",
                                         "Name", "Type", "LABPC[ID]"]
                        for b in bs:
                            LABPC = get_value_validate(b, "LABPC")
                            holders = get_value_validate(b, "holders")
                            holders = [h['username'] for h in holders]
                            x.add_row([get_value_validate(b, "board_id"), get_value_validate(b, "name"), get_value_validate(b, "type"),
                                       "%s[%s]" % (get_value_validate(LABPC, "username"), get_value_validate(LABPC, "id"))])
                    print(x)
            else:
                raise Exception(resp.get("error"))
        except ValueError as e:
            print(str(e))
            parser.print_help()
        except Exception as e:
            print({"ok": False, "error": str(e)})

    def status(self):
        """Execute command to find the expected board status by using filter

        """
        parser = argparse.ArgumentParser(
            description='Filter the sharing board status',
            usage='''status [<board_id> | [<filter>: [-b/--board-id] [-t/--board-type]
                            [-l/--labpc-id]]]
                            [-h/--help] [-f/--format] [-e]

Display status boards is sharing on system.

Example:
status             Status all board is sharing on system.
status 1           Status specific board with unique id=1 .

with some <filter>:
status -b 1             Status specific board with board unique id (board_id=1).
status -l 2             Status all board on specific labpc(labpc_id=2).
status -l 3 -t S4       Status all board in specific labPC (labpc_id=3) and type board(board_type=S4).

''')
        parser.add_argument('status', action='append', nargs="+")
        parser.add_argument('-b', '--board-id', help='Add Board UID into search parameters.')
        parser.add_argument('--board-pos', help='Add Board position into search parameters')
        parser.add_argument('-l', '--labpc-id', help='Add LabPC ID to search parameters')
        parser.add_argument('-t', '--board-type', help='Add Board type into search parameters')
        parser.add_argument('-u', '--using-by', nargs='?',
                            const=os.getlogin(), help='Indicates who the device is used by.')
        parser.add_argument('-f', '--format', default='table',
                            help='format type, json or table.')
        parser.add_argument('-e', dest = "exp", help='Display expand data.',
                            nargs='?', const=True, default=False)

        try:
            args = parser.parse_args(sys.argv[1:])
            if len(args.status[0]) == 2:
                if args.board_id is None:
                    args.board_id = args.status[0][1]
                else:
                    raise ValueError("Unrecognized command")

            params = {}
            set_if_not_none(params, "board_id", args.board_id)
            set_if_not_none(params, "labpc_id", args.labpc_id)
            set_if_not_none(params, "board_type", args.board_type)
            set_if_not_none(params, "using_by", args.using_by)
            set_if_not_none(params, "board_pos", args.board_pos)

            try:
                resp = requests.get(
                    self.cfg.url_status_board_share, params=params, headers=self.cfg.headers).json()
            except:
                raise Exception(
                    "Cannot connect to server. Change settings in config.json and try again.")
            ok = resp.get("ok", None)
            if ok is None:
                raise Exception(str(resp))
            bs = resp.get("boards")
            nbs = []
            if ok:
                nbs = bs
            else:  # error
                for b in bs:
                    board_error_detected = False
                    for e in resp.get("error"):
                        if b["board_id"] == e["board_id"]:
                            board_error_detected = True
                            nbs.append(e)
                            break
                    if not board_error_detected:
                        nbs.append(b)
            if args.format == 'json':
                print(json.dumps(nbs, indent=4, sort_keys=True))
            else:
                x = PrettyTable()
                x.junction_char = JUNCTION_CHAR
                if args.exp:
                    x.field_names = ["Board UID", "Name", "Type", "LABPC[ID]",
                                     "LABPC Address", "Power", "Boot", "Board Pos"]
                    for b in nbs:
                        x.add_row(
                            [get_value_validate(b, "board_id"), get_value_validate(b, "name"), get_value_validate(b, "type"),
                             "%s[%s]" % (get_value_validate(b, "LABPC_owner"), get_value_validate(b, "LABPC_id")), "%s:%s" % (
                                 get_value_validate(b, "LABPC_host"), get_value_validate(b, "LABPC_port")),
                             get_value_validate(b, "status_power"), get_value_validate(b, "status_boot"), get_value_validate(b, "board_pos")]
                        )
                else:
                    x.field_names = ["Board UID", "Name",
                                     "Type", "LABPC[ID]", "Power", "Boot"]
                    for b in nbs:
                        x.add_row(
                            [get_value_validate(b, "board_id"), get_value_validate(b, "name"), get_value_validate(b, "type"), "%s[%s]" % (get_value_validate(b, "LABPC_owner"),
                                                                                                                                          get_value_validate(b, "LABPC_id")), get_value_validate(b, "status_power"), get_value_validate(b, "status_boot")]
                        )
                print(x)
        except ValueError as e:
            print(str(e))
            parser.print_help()
        except Exception as e:
            print({"ok": False, "error": str(e)})

    def control(self):
        """Execute command to control the board's power state
        """
        parser = argparse.ArgumentParser(
            usage='''<command> [<board_id> | [-b/--board-id | -l/--labpc & --board-pos]]
                               [-h/--help]


Control specific board in boardfarm.
Command is one of on/off/boot/unboot.

Example:
$python main.py on 1                        
or $python main.py on -b 1                  Turn on board with board unique id (board_id=1).
$python main.py boot -l 6 --board-pos 2     Boot specific a board with: labpc_id=6 and board position in labPC is 2.


''')
        # prefixing the argument with -- means it's optional
        parser.add_argument('control', action='append', nargs="+")
        parser.add_argument('-b', '--board-id', help='Board UID.')
        parser.add_argument('--board-pos', help='Board position in labPC.')
        parser.add_argument('-l', '--labpc-id', help='LabPC ID')
        parser.add_argument('-r', '--relay', help='Relay number')
        parser.add_argument('-u', '--using-by', nargs='?', const=os.getlogin(), help='''
        This option indicates who the device is used by. 
        '''
                            )
        try:
            # parsing the argument
            args = parser.parse_args(sys.argv[1:])
            if len(args.control[0]) == 2:
                if args.board_id is None:
                    args.board_id = args.control[0][1]
                else:
                    raise ValueError("Unrecognized command")
            control = args.control[0][0]
            if control == None:
                raise Exception("Missing control command:on/off/boot/unboot.")
            uparams = {}
            if args.using_by is not None:
                if control == "on":
                    set_if_not_none(uparams, "occupy", "take")
                elif control == "off":
                    set_if_not_none(uparams, "occupy", "give")
            set_if_not_none(uparams, "using_by", args.using_by)

            if None not in [args.labpc_id, args.board_pos] and all(e == None for e in [args.board_id, args.relay]):
                p = {"control": control, 'labpc_id': args.labpc_id,
                     'board_pos': int(args.board_pos)}
                p.update(uparams)
            elif None not in [args.board_id] and all(e == None for e in [args.labpc_id, args.relay, args.board_pos]):
                p = {'board_id': int(args.board_id), "control": control}
                p.update(uparams)
            else:
                raise ValueError("Unrecognized command")
            try:
                resp = requests.get(
                    self.cfg.url_control_board_share, params=p, headers=self.cfg.headers).json()
                ok = resp.get("ok", None)
                if ok is None:
                    raise Exception(str(resp))
                if ok:
                    resp = {"ok": True, "message": resp.get("message")}
                else:
                    resp = {"ok": False, "error": resp.get("error")}
            except Exception:
                raise Exception("Cannot get response from server")
            print(resp)
        except ValueError as e:
            print(str(e))
            parser.print_help()
        except Exception as e:
            print({"ok": False, "error": str(e)})


if __name__ == '__main__':
    RemoteCLIArgumentParser()
